<?php

namespace Drupal\news\Controller;

use Dotenv\Dotenv;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class NewsController extends ControllerBase
{
  public function topStories(): array
  {
    $client = new Client();
    $dotenv = Dotenv::createImmutable(dirname(__DIR__, 4))->load();
    $topStories = [];
    $topStoriesComments = [];

    try {
      $topStoriesIds = json_decode($client->get($dotenv['HACKER_NEWS_API'] . 'topstories.json')->getBody()->getContents());

      foreach ($topStoriesIds as $topStoriesId) {
        $topStoriesObject = json_decode($client->get($dotenv['HACKER_NEWS_API'] . '/item/' . $topStoriesId . '.json')->getBody()->getContents());
        $storyComments = $topStoriesObject->kids ?? [];

        foreach ($storyComments as $storyComment) {
          $topStoriesCommObj = json_decode($client->get($dotenv['HACKER_NEWS_API'] . '/item/' . $storyComment . '.json')->getBody()->getContents());
          if (true !== $topStoriesCommObj->deleted) {
            $topStoriesComments[] = [
              'by' => $topStoriesCommObj->by ?? '',
              'text' => $topStoriesCommObj->text ?? '',
              'type' => $topStoriesCommObj->type ?? '',
              'time' => date('Y-m-d H:m:s', $topStoriesCommObj->time) ?? ''
            ];
          }

          /** @var Connection $connection */
          $connection = \Drupal::service('database');

          $connection->insert('top_stories_comments')->fields([
            'by' => $topStoriesCommObj->by ?? '',
            'type' => $topStoriesCommObj->type ?? '',
            'time' => $topStoriesCommObj->time ?? '',
            'text' => $topStoriesCommObj->text ?? '',
            'parent' => $topStoriesCommObj->parent ?? '',
            'kids' => $topStoriesCommObj->kids ?? '',
            'id' => $topStoriesCommObj->id ?? '',
          ])->execute();
        }

        $topStories[] = [
          'id' => $topStoriesObject->id ?? '',
          'title' => $topStoriesObject->title ?? '',
          'by' => $topStoriesObject->by ?? '',
          'time' => date('Y-m-d H:i:s', $topStoriesObject->time) ?? '',
          'type' => $topStoriesObject->type ?? '',
          'url' => $topStoriesObject->url ?? '',
          'deleted' => $topStoriesObject->deleted ?? '',
          'dead' => $topStoriesObject->dead ?? '',
          'parent' => $topStoriesObject->parent ?? '',
          'poll' => $topStoriesObject->poll ?? '',
          'score' => $topStoriesObject->score ?? '',
          'parts' => $topStoriesObject->parts ?? '',
          'descendants' => $topStoriesObject->descendants ?? '',
          'storyComments' => $topStoriesComments ?? []
        ];

        /** @var Connection $connection */
        $connection = \Drupal::service('database');
        $connection->insert('top_stories')->fields([
          'story_id' => $topStoriesId,
          'descendants' => $topStoriesId->descendants ?? '',
          'kids' => $topStoriesId->kids ?? '',
          'score' => $topStoriesId->score ?? '',
          'time' => $topStoriesId->time ?? '',
          'title' => $topStoriesId->title ?? '',
          'type' => $topStoriesId->type ?? '',
          'url' => $topStoriesId->url ?? '',
        ])->execute();
      }
    } catch (ClientException $e) {
      if ($e->hasResponse()) {
        return [
          '#theme' => 'top_stories_theme',
          '#stories' => null,
          '#messages' => [json_decode($e->getResponse()->getBody()->getContents())->error]
        ];
      }
    } catch (\Exception $exception) {
      return [
        '#theme' => 'top_stories_theme',
        '#stories' => null,
        '#messages' => [$exception->getMessage()]
      ];
    }

    if (empty($topStories)) {
      return [
        '#theme' => 'top_stories_theme',
        '#stories' => null,
        '#messages' => ['Oops!!! Unfortunately stories seem to be unavailable at the moment. Please try again later']
      ];
    }

    return [
      '#theme' => 'top_stories_theme',
      '#stories' => $topStories,
      '#messages' => null
    ];
  }

  public function newStories(): array
  {
    $client = new Client();
    $dotenv = Dotenv::createImmutable(dirname(__DIR__, 4))->load();
    $newStories = [];
    $newStoriesComments = [];

    try {
      $newStoriesIds = json_decode($client->get($dotenv['HACKER_NEWS_API'] . 'newstories.json')->getBody()->getContents());

      foreach ($newStoriesIds as $newStoriesId) {
        $newStoriesObject = json_decode($client->get($dotenv['HACKER_NEWS_API'] . '/item/' . $newStoriesId . '.json')->getBody()->getContents());
        $newComments = $newStoriesObject->kids ?? [];

        foreach ($newComments as $newComment) {
          $newStoriesCommObj = json_decode($client->get($dotenv['HACKER_NEWS_API'] . '/item/' . $newComment . '.json')->getBody()->getContents());
          if (true !== $newStoriesCommObj->deleted) {
            $newStoriesComments[] = [
              'by' => $newStoriesCommObj->by ?? '',
              'text' => $newStoriesCommObj->text ?? '',
              'type' => $newStoriesCommObj->type ?? '',
              'time' => date('Y-m-d H:m:s', $newStoriesCommObj->time) ?? ''
            ];
          }

          /** @var Connection $connection */
          $connection = \Drupal::service('database');

          $connection->insert('new_stories_comments')->fields([
            'by' => $newStoriesCommObj->by ?? '',
            'type' => $newStoriesCommObj->type ?? '',
            'time' => $newStoriesCommObj->time ?? '',
            'text' => $newStoriesCommObj->text ?? '',
            'parent' => $newStoriesCommObj->parent ?? '',
            'kids' => $newStoriesCommObj->kids ?? '',
            'id' => $newStoriesCommObj->id ?? '',
          ])->execute();
        }

        $newStories[] = [
          'id' => $newStoriesObject->id ?? '',
          'title' => $newStoriesObject->title ?? '',
          'by' => $newStoriesObject->by ?? '',
          'time' => date('Y-m-d H:i:s', $newStoriesObject->time) ?? '',
          'type' => $newStoriesObject->type ?? '',
          'url' => $newStoriesObject->url ?? '',
          'deleted' => $newStoriesObject->deleted ?? '',
          'dead' => $newStoriesObject->dead ?? '',
          'parent' => $newStoriesObject->parent ?? '',
          'poll' => $newStoriesObject->poll ?? '',
          'score' => $newStoriesObject->score ?? '',
          'parts' => $newStoriesObject->parts ?? '',
          'descendants' => $newStoriesObject->descendants ?? '',
          'storyComments' => $newStoriesComments ?? []
        ];

        /** @var Connection $connection */
        $connection = \Drupal::service('database');
        $connection->insert('new_stories')->fields([
          'story_id' => $newStoriesId,
          'descendants' => $newStoriesId->descendants ?? '',
          'kids' => $newStoriesId->kids ?? '',
          'score' => $newStoriesId->score ?? '',
          'time' => $newStoriesId->time ?? '',
          'title' => $newStoriesId->title ?? '',
          'type' => $newStoriesId->type ?? '',
          'url' => $newStoriesId->url ?? '',
        ])->execute();
      }
    } catch (ClientException $e) {
      if ($e->hasResponse()) {
        return [
          '#theme' => 'new_stories_theme',
          '#stories' => null,
          '#messages' => [json_decode($e->getResponse()->getBody()->getContents())->error]
        ];
      }
    } catch (\Exception $exception) {
      return [
        '#theme' => 'new_stories_theme',
        '#stories' => null,
        '#messages' => [$exception->getMessage()]
      ];
    }

    if (empty($newStories)) {
      return [
        '#theme' => 'new_stories_theme',
        '#stories' => null,
        '#messages' => ['Oops!!! Unfortunately new stories seem to be unavailable at the moment. Please try again later']
      ];
    }

    return [
      '#theme' => 'new_stories_theme',
      '#stories' => $newStories,
      '#messages' => null
    ];
  }

  public function bestStories(): array
  {
    $client = new Client();
    $dotenv = Dotenv::createImmutable(dirname(__DIR__, 4))->load();
    $bestStories = [];
    $bestStoriesComments = [];

    try {
      $bestStoriesIds = json_decode($client->get($dotenv['HACKER_NEWS_API'] . 'beststories.json')->getBody()->getContents());

      foreach ($bestStoriesIds as $bestStoriesId) {
        $bestStoriesObject = json_decode($client->get($dotenv['HACKER_NEWS_API'] . '/item/' . $bestStoriesId . '.json')->getBody()->getContents());
        $bestComments = $bestStoriesObject->kids ?? [];

        foreach ($bestComments as $bestComment) {
          $bestStoriesCommObj = json_decode($client->get($dotenv['HACKER_NEWS_API'] . '/item/' . $bestComment . '.json')->getBody()->getContents());
          if (true !== $bestStoriesCommObj->deleted) {
            $bestStoriesComments[] = [
              'by' => $bestStoriesCommObj->by ?? '',
              'text' => $bestStoriesCommObj->text ?? '',
              'type' => $bestStoriesCommObj->type ?? '',
              'time' => date('Y-m-d H:m:s', $bestStoriesCommObj->time) ?? ''
            ];
          }

          /** @var Connection $connection */
          $connection = \Drupal::service('database');

          $connection->insert('best_stories_comments')->fields([
            'by' => $bestStoriesCommObj->by ?? '',
            'type' => $bestStoriesCommObj->type ?? '',
            'time' => $bestStoriesCommObj->time ?? '',
            'text' => $bestStoriesCommObj->text ?? '',
            'parent' => $bestStoriesCommObj->parent ?? '',
            'kids' => $bestStoriesCommObj->kids ?? '',
            'id' => $bestStoriesCommObj->id ?? '',
          ])->execute();
        }

        $bestStories[] = [
          'id' => $bestStoriesObject->id ?? '',
          'title' => $bestStoriesObject->title ?? '',
          'by' => $bestStoriesObject->by ?? '',
          'time' => date('Y-m-d H:i:s', $bestStoriesObject->time) ?? '',
          'type' => $bestStoriesObject->type ?? '',
          'url' => $bestStoriesObject->url ?? '',
          'deleted' => $bestStoriesObject->deleted ?? '',
          'dead' => $bestStoriesObject->dead ?? '',
          'parent' => $bestStoriesObject->parent ?? '',
          'poll' => $bestStoriesObject->poll ?? '',
          'score' => $bestStoriesObject->score ?? '',
          'parts' => $bestStoriesObject->parts ?? '',
          'descendants' => $bestStoriesObject->descendants ?? '',
          'storyComments' => $besttoriesComments ?? []
        ];

        /** @var Connection $connection */
        $connection = \Drupal::service('database');
        $connection->insert('best_stories')->fields([
          'story_id' => $bestStoriesId,
          'descendants' => $bestStoriesId->descendants ?? '',
          'kids' => $bestStoriesId->kids ?? '',
          'score' => $bestStoriesId->score ?? '',
          'time' => $bestStoriesId->time ?? '',
          'title' => $bestStoriesId->title ?? '',
          'type' => $bestStoriesId->type ?? '',
          'url' => $bestStoriesId->url ?? '',
        ])->execute();
      }
    } catch (ClientException $e) {
      if ($e->hasResponse()) {
        return [
          '#theme' => 'best_stories_theme',
          '#stories' => null,
          '#messages' => [json_decode($e->getResponse()->getBody()->getContents())->error]
        ];
      }
    } catch (\Exception $exception) {
      return [
        '#theme' => 'best_stories_theme',
        '#stories' => null,
        '#messages' => [$exception->getMessage()]
      ];
    }

    if (empty($bestStories)) {
      return [
        '#theme' => 'best_stories_theme',
        '#stories' => null,
        '#messages' => ['Oops!!! Unfortunately best stories seem to be unavailable at the moment. Please try again later']
      ];
    }

    return [
      '#theme' => 'best_stories_theme',
      '#stories' => $bestStories,
      '#messages' => null
    ];
  }
}
