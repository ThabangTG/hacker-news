# Create best_stories table
create table best_stories
(
  id          int auto_increment,
  story_id    int          not null,
  descendants varchar(200) null,
  kids        longtext     null,
  score       varchar(10)  null,
  time        varchar(50)  null,
  title       longtext     null,
  type        varchar(20)  null,
  url         longtext     null,
  constraint best_stories_id_uindex
    unique (id)
);

alter table best_stories
  add primary key (id);


# Create top_stories table
create table top_stories
(
  id          int auto_increment,
  story_id    int          not null,
  descendants varchar(200) null,
  kids        longtext     null,
  score       varchar(10)  null,
  time        varchar(50)  null,
  title       longtext     null,
  type        varchar(20)  null,
  url         longtext     null,
  constraint top_stories_id_uindex
    unique (id)
);

alter table top_stories
  add primary key (id);

# Create new_stories table
create table new_stories
(
  id          int auto_increment,
  story_id    int          not null,
  descendants varchar(200) null,
  kids        longtext     null,
  score       varchar(10)  null,
  time        varchar(50)  null,
  title       longtext     null,
  type        varchar(20)  null,
  url         longtext     null,
  constraint new_stories_id_uindex
    unique (id)
);

alter table best_stories
  add primary key (id);
