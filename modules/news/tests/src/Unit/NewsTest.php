<?php

namespace Drupal\Tests\news\Unit;

use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;

class NewsTest extends UnitTestCase
{
    public function testNewStories()
    {
        $client = new Client();
        $newStories = json_decode($client->get('https://hacker-news.firebaseio.com/v0/newstories.json')->getBody()->getContents());
        $unauthErrorEndpoint = json_decode($client->get('https://hacker-news.firebaseio.com/v0/newstories.json')->getBody()->getContents());

        $unauthError = (object) [
            "error" => "Permission denied"
        ];

        $this->assertIsArray($newStories);
        $this->assertIsObject($unauthErrorEndpoint);
        $this->assertSame($unauthErrorEndpoint, $unauthError);
    }
}
